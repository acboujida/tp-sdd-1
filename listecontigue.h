#ifndef LISTECONTIGUE__H
#define LISTECONTIGUE__H

#include <stdio.h>
#include <stdlib.h>
#include "semaines.h"

#define CAPACITE 100

typedef struct listecontigue
{
    int capacite;
    char* tete;
    char* fin;
}listco_t;

listco_t* InitialiserListeContigue(int cap);
void LibererListeContigue(listco_t* liste);
void AfficherDate(char* date);
void AfficherListeContigue(listco_t* liste);
void LibererListeContigue(listco_t* liste);
listco_t* InsererDate(listco_t* liste, char* date);
listco_t* CreerListe(sem_t* ListeSemaines, char* motif);
int estVide(listco_t* liste);
int ExisteDansListeContigue(listco_t* liste, char* date);

#endif // LISTECONTIGUE__H