#include "semaines.h"

/*AfficherListeSemaines est une fonction qui permet d'afficher tout le contenue de notre liste chaînée semaine ainsi que les actions liés a chaque semaine
entrées : le pointeur de tête de la liste de type semaine
pas de sortie*/

void AfficherListeSemaines(sem_t* ListeSemaines)
{
    sem_t* cour = ListeSemaines;
    while (cour != NULL)
    {
        printf("Année : %s, Semaine : %s\n", cour->annee, cour->semaine);
        AfficherListeActions(cour->actions);
        printf("-----------------------------------------------------\n");
        cour = cour->suivant;
    }
}

/*InitialiserListeSemaines est la fonction que l'on utilise afin d'allouer la mémoire nécessaire a la création d'une nouvelle cellule de notre liste chaînée semaine,puis d'initialiser cette nouvelle cellule.
entrées : pointeur menant vers la liste des actions de cette semaine de type action
sortie : nouvelle cellule semaine*/

sem_t* InitialiserListeSemaines(act_t* action)
{
    sem_t* semaine = malloc(sizeof(sem_t));
    if (semaine != NULL)
    {
        strcpy(semaine->annee, "\0");
        strcpy(semaine->semaine, "\0");
        semaine->actions = action;
        semaine->suivant = NULL;
    }

    return semaine;
}

/*LireDuFichier, cette fonction nous permet de lire le fichier texte en entrée et d'en copier le contenue dans notre liste chaînée semaine.
entrées : une chaîne de caractères contenant le nom du fichier texte a lire nommé NomFichier; le pointeur de tête de notre liste chaînée semaine nommé ListeSemaines.
sortie : pas de sortie*/

sem_t* LireDuFichier(char* NomFichier)
{
    sem_t* ListeSemaines;
    FILE* fic = fopen(NomFichier, "r");
    if (fic != NULL)
    {
        char buffer[N];
        fgets(buffer, 20, fic);
        ListeSemaines = LectureLigne(buffer);
        strcpy(buffer, "\0");
        fseek(fic, 1, SEEK_CUR);
        while (fgets(buffer, 20, fic) != NULL)
        {
            ListeSemaines = Ajouter(buffer, ListeSemaines);
            strcpy(buffer, "\0");
            fseek(fic, 1, SEEK_CUR);
        }
        fclose(fic);
    }
    return ListeSemaines;
}

sem_t* RechercheSemaine(sem_t* semaine, sem_t* ele_recherche, int* code)
{
    sem_t* cour = semaine;
    sem_t* prec = NULL;
    *code = 0;
    while ((cour != NULL) && (strncmp(cour->annee, ele_recherche->annee, 4*sizeof(char)) < 0))
    {
        prec = cour;
        cour = cour->suivant;
    }
    while ((cour != NULL) && (strncmp(cour->semaine, ele_recherche->semaine, 2*sizeof(char)) < 0))
    {
        prec = cour;
        cour = cour->suivant;
    }

    if ((cour != NULL) && (strncmp(cour->semaine, ele_recherche->semaine, 2*sizeof(char)) == 0) && (strncmp(cour->annee, ele_recherche->annee, 4*sizeof(char)) == 0))
    {
        //prec = cour;
        *code = 1;
    }

    return prec;
}

sem_t* InsererSemaine(sem_t* semaine, sem_t* a_insere)
{
    sem_t* tete = semaine;
    int trouvee = 0;
    sem_t* prec = RechercheSemaine(semaine, a_insere, &trouvee);
    if (trouvee)
    {
        if (prec == NULL)
        {
            semaine->actions = InsererAction(semaine->actions, a_insere->actions);
            free(a_insere);
        }
        else
        {
            prec->suivant->actions = InsererAction(prec->suivant->actions, a_insere->actions);
            free(a_insere);
        }
    }
    else
    {
        if (prec != NULL)
        {
            a_insere->suivant = prec->suivant;
            prec->suivant = a_insere;
        }
        else
        {
            a_insere->suivant = semaine;
            tete = a_insere;
        }
    }

    return tete;
}

sem_t* Ajouter(char* chaine, sem_t* ListeSemaines)
{
    sem_t* semaine = LectureLigne(chaine);
    ListeSemaines = InsererSemaine(ListeSemaines, semaine);

    return ListeSemaines;
}


sem_t* LectureLigne(char* chaine)
{
    sem_t* semaine = NULL;
    act_t* action = InitialiserListeActions();
    if (action != NULL)
    {
        action->jour = chaine[6];
        strncpy(action->heure, chaine+7, 2);
        action->heure[2] = '\0';
        strncpy(action->NomAction, chaine+9, 10);
        action->NomAction[10] = '\0';
        semaine = InitialiserListeSemaines(action);
        if (semaine != NULL)
        {
            strncpy(semaine->annee, chaine, 4);
            semaine->annee[4]='\0';
            strncpy(semaine->semaine, chaine+4, 2);
            semaine->semaine[2] = '\0';
        }
    }

    return semaine;
}

void LibererSemaine(sem_t* semaine)
{
    sem_t* cour = semaine;
    sem_t* sauv;
    while (cour != NULL)
    {
        LibererAction(cour->actions);
        sauv = cour->suivant;
        free(cour);
        cour = sauv;
    }
}

void EcrireSDD(sem_t* ListeSemaines, char* NomFichier)
{
    FILE* fic = fopen(NomFichier, "w");
    if (fic != NULL)
    {
        sem_t* cour1 = ListeSemaines;
        act_t* cour2;
        while (cour1 != NULL)
        {
            cour2 = cour1->actions;
            while (cour2 != NULL)
            {
                fprintf(fic, "%s%s", cour1->annee, cour1->semaine);
                fprintf(fic, "%c%s%s", cour2->jour, cour2->heure, cour2->NomAction);
                cour2 = cour2->suivant;
                if ((cour2 != NULL) || (cour1->suivant != NULL))
                {
                    fprintf(fic, "\n");
                }
            }
            cour1 = cour1->suivant;
        }
        fclose(fic);
    }
}

sem_t* SupprimerSemaine(sem_t* ListeSemaines, char* annee, char* semaine, char jour, char* heure)
{
    int trouvee = 0;
    act_t* action = InitialiserListeActions();
    action->jour = jour;
    strncpy(action->heure, heure, 2);
    sem_t* sem_asupprime = InitialiserListeSemaines(action);
    strncpy(sem_asupprime->annee, annee, 4);
    strncpy(sem_asupprime->semaine, semaine, 2);
    sem_t* prec = RechercheSemaine(ListeSemaines, sem_asupprime, &trouvee);
    int nbreactions = 0;
    sem_t* sauv;

    if (trouvee)
    {
        if (prec == NULL)
        {
            nbreactions = NombreActionsDansSemaine(ListeSemaines);
            if (nbreactions == 1)
            {
                sauv = ListeSemaines;
                ListeSemaines = ListeSemaines->suivant;
                sauv->suivant = NULL;
                LibererSemaine(sauv);
            }
            else
            {
                ListeSemaines->actions = SupprimerAction(ListeSemaines->actions, jour, heure);
            }
        }
        else
        {
            nbreactions = NombreActionsDansSemaine(prec->suivant);
            if (nbreactions == 1)
            {
                sauv = prec->suivant;
                prec->suivant = sauv->suivant;
                sauv->suivant = NULL;
                LibererSemaine(sauv);
            }
            else
            {
                prec->suivant->actions = SupprimerAction(prec->suivant->actions, jour, heure);
            }
        }
    }

    LibererSemaine(sem_asupprime);

    return ListeSemaines;
}

int NombreActionsDansSemaine(sem_t* semaine)
{
    act_t* cour = semaine->actions;
    int i = 0;
    while (cour != NULL)
    {
        cour = cour->suivant;
        i++;
    }

    return i;
}
