#include "listecontigue.h"

int main(int argc, char* argv[])
{
    if (argc == 2)
    {
        sem_t* sem = LireDuFichier(argv[1]);

        AfficherListeSemaines(sem);

        sem = SupprimerSemaine(sem, "2020", "11", '1', "08");

        EcrireSDD(sem, "file.txt");

        listco_t* liste = CreerListe(sem, "TP");

        AfficherListeContigue(liste);

        LibererSemaine(sem);

        LibererListeContigue(liste);
    }

    return 0;
}
