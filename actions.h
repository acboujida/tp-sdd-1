#ifndef ACTIONS__H
#define ACTIONS__H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX 10

typedef struct listeactions
{
    char jour;
    char heure[2];
    struct listeactions* suivant;
    char NomAction[10];
} act_t;

act_t* InitialiserListeActions();
void AfficherListeActions(act_t* ListeActions);
act_t* RechercheAction(act_t* action, act_t* ele_recherche, int* code);
act_t* InsererAction(act_t* action, act_t* a_insere);
void LibererAction(act_t* action);
int ExisteMotif(act_t* action, char* motif);
act_t* SupprimerAction(act_t* ListeActions, char jour, char* heure);

#endif // ACTIONS__H