#include "listecontigue.h"

listco_t* InitialiserListeContigue(int cap)
{
    listco_t* liste = malloc(sizeof(listco_t));
    if (liste != NULL)
    {
        liste->capacite = cap;
        liste->tete = malloc(liste->capacite * 9 * sizeof(char*));
        if (liste->tete != NULL)
        {
            liste->tete[0] = '\0';
            liste->fin = liste->tete;
        }
    }

    return liste;
}

void AfficherDate(char* date)
{
    int i = 0;
    printf("ANNEE : ");
    while (i < 4)
    {
        printf("%c", date[i]);
        i++;
    }
    printf(" | SEMAINE : ");
    while (i < 6)
    {
        printf("%c", date[i]);
        i++;
    }
    printf(" | JOUR : %c", date[i]);
    i++;
    printf(" | HEURE : ");
    while (i < 9)
    {
        printf("%c", date[i]);
        i++;
    }
    printf("\n");
}

void AfficherListeContigue(listco_t* liste)
{
    char* cour = liste->tete;
    if (!estVide(liste))
    {
        while (cour <= liste->fin)
        {
            AfficherDate(cour);
            cour = cour + 9;
        }
    }
}

void LibererListeContigue(listco_t* liste)
{
    free(liste->tete);
    free(liste);
}

listco_t* InsererDate(listco_t* liste, char* date)
{
    if (estVide(liste))
    {
        strncpy(liste->tete, date, 4);
        strncpy(liste->tete+4, date+4, 2);
        liste->tete[6] = date[6];
        strncpy(liste->tete+7, date+7, 2);
    }
    else
    {
        if (!ExisteDansListeContigue(liste, date))
        {
            liste->fin = liste->fin + 9;
            strncpy(liste->fin, date, 4);
            strncpy(liste->fin+4, date+4, 2);
            liste->fin[6] = date[6];
            strncpy(liste->fin+7, date+7, 2);
        }
    }

    return liste;
}

listco_t* CreerListe(sem_t* ListeSemaines, char* motif)
{
    listco_t* liste = InitialiserListeContigue(CAPACITE);
    sem_t* cour1 = ListeSemaines;
    act_t* cour2;
    char date[9] = {};
    while (cour1 != NULL)
    {
        cour2 = cour1->actions;
        while (cour2 != NULL)
        {
            if (ExisteMotif(cour2, motif))
            {
                strncpy(date, cour1->annee, 4);
                strncpy(date+4, cour1->semaine, 2);
                date[6] = cour2->jour;
                strncpy(date+7, cour2->heure, 2);
                liste = InsererDate(liste, date);
            }
            cour2 = cour2->suivant;
        }
        cour1 = cour1->suivant;
    }

    return liste;
}

int estVide(listco_t* liste)
{
    return ((liste->fin == liste->tete) && (liste->tete[0] == '\0'));
}

int ExisteDansListeContigue(listco_t* liste, char* date)
{
    char* cour = liste->tete;
    int res = 0;
    if (!estVide(liste))
    {
        while ((cour <= liste->fin) && (!res))
        {
            if (strncmp(cour, date, 9) == 0)
            {
                res = 1;
            }
            cour = cour + 9;
        }
    }
    return res;
}