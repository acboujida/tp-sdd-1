#ifndef SEMAINES__H
#define SEMAINES__H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"

#define N 19

typedef struct listesemaines
{
    char annee[4];
    act_t* actions;
    char semaine[2];
    struct listesemaines* suivant;
}sem_t;

sem_t* InitialiserListeSemaines(act_t* action);
void AfficherListeSemaines(sem_t* ListeSemaines);
sem_t* LireDuFichier(char* NomFichier);
sem_t* RechercheSemaine(sem_t* semaine, sem_t* ele_recherche, int* code);
sem_t* InsererSemaine(sem_t* semaine, sem_t* a_insere);
sem_t* LectureLigne(char* chaine);
sem_t* Ajouter(char* chaine, sem_t* ListeSemaines);
void LibererSemaine(sem_t* semaine);
void EcrireSDD(sem_t* ListeSemaines, char* NomFichier);
sem_t* SupprimerSemaine(sem_t* ListeSemaines, char* annee, char* semaine, char jour, char* heure);
int NombreActionsDansSemaine(sem_t* semaine);

#endif // SEMAINES__H