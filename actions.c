#include "actions.h"

void AfficherListeActions(act_t* ListeActions)
{
    act_t* cour = ListeActions;
    while (cour != NULL)
    {
        printf("Jour : %c, Heure : %s\n", cour->jour, cour->heure);
        printf("-> Actions : %s\n", cour->NomAction);
        cour = cour->suivant;
    }
}

act_t* InitialiserListeActions()
{
    act_t* action = malloc(sizeof(act_t));
    if (action != NULL)
    {
        action->jour = 0;
        strcpy(action->heure, "\0");
        strcpy(action->NomAction, "\0");
        action->suivant = NULL;
    }

    return action;
}

act_t* RechercheAction(act_t* action, act_t* ele_recherche, int* code)
{
    act_t* prec = NULL;
    act_t* cour = action;
    while ((cour != NULL) && (cour->jour < ele_recherche->jour))
    {
        prec = cour;
        cour = cour->suivant;
    }
    while ((cour != NULL) && (strncmp(cour->heure, ele_recherche->heure, 2*sizeof(char)) < 0))
    {
        prec = cour;
        cour = cour->suivant;
    }

    if ((cour != NULL) && (strncmp(cour->heure, ele_recherche->heure, 2*sizeof(char)) == 0) && (cour->jour == ele_recherche->jour))
    {
        //prec = cour;
        *code = 1;
    }

    return prec;
}

act_t* InsererAction(act_t* action, act_t* a_insere)
{
    act_t* tete = action;
    int trouvee = 0;
    act_t* prec = RechercheAction(action, a_insere, &trouvee);
    if (prec == NULL)
    {
        a_insere->suivant = action;
        tete = a_insere;
    }
    else if (!trouvee)
    {
        a_insere->suivant = prec->suivant;
        prec->suivant = a_insere;
    }

    return tete;
}

void LibererAction(act_t* action)
{
    act_t* cour = action;
    act_t* sauv;
    while (cour != NULL)
    {
        sauv = cour->suivant;
        free(cour);
        cour = sauv;
    }
}

act_t* SupprimerAction(act_t* ListeActions, char jour, char* heure)
{
    int trouvee;
    act_t* action_asupprime = InitialiserListeActions();
    action_asupprime->jour = jour;
    strncpy(action_asupprime->heure, heure, 2);
    act_t* prec = RechercheAction(ListeActions, action_asupprime, &trouvee);
    act_t* sauv;

    if (trouvee)
    {
        if (prec == NULL)
        {
            sauv = ListeActions;
            ListeActions = ListeActions->suivant;
            sauv->suivant = NULL;
            LibererAction(sauv);
        }
        else
        {
            sauv = prec->suivant;
            prec->suivant = sauv->suivant;
            sauv->suivant = NULL;
            LibererAction(sauv);
        }
    }

    LibererAction(action_asupprime);

    return ListeActions;
}

int ExisteMotif(act_t* action, char* motif)
{
    int n = strlen(motif);
    int i = 0;
    int res = 0;
    while ((action->NomAction[i] != '\0') && (!res))
    {
        if (strncmp(action->NomAction+i, motif, n) == 0)
        {
            res = 1;
        }
        i++;
    }

    return res;
}