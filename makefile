CC = gcc
OPT = -Wextra -Wall -g
OBJ = main.o actions.o semaines.o listecontigue.o
prog : $(OBJ)
	$(CC) $(OBJ) $(OPT) -o prog
	@echo "lancer le programme avec ./prog fichier.txt"
main.o : main.c actions.c actions.h semaines.c semaines.h
	$(CC) $(OPT) -c main.c
actions.o : actions.c actions.h
	$(CC) $(OPT) -c actions.c
semaines.o : semaines.c semaines.h
	$(CC) $(OPT) -c semaines.c
listecontigue.o : listecontigue.c listecontigue.h
	$(CC) $(OPT) -c listecontigue.c
clean :
	rm $(OBJ)